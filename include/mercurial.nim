import os
import osproc
import strformat

import ../src/exceptions

proc mercurial*(args: openArray[string]): bool =
  ## Invoke the mercurial(aka:hg) program found on the system path
  let mercurial = findExe("hg")
  if mercurial.len == 0:
    raise newException(SubprocException, "Cannot locate mercurial(aka:hg) on system path")

  let mercurialproc = startProcess(mercurial, getCurrentDir(), args, options={poParentStreams})
  if waitForExit(mercurialproc) == 0: return true

