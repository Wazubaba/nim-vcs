import os
import osproc
import strformat

import ../src/exceptions

proc git*(args: openArray[string]): bool =
  ## Invoke the git program found on the system path
  let git = findExe("git")
  if git.len == 0:
    raise newException(SubprocException, "Cannot locate git on system path")

  let gitproc = startProcess(git, getCurrentDir(), args, options={poParentStreams})
  if waitForExit(gitproc) == 0: return true

proc clone*(url, path: string): bool =
  ## Download a repository normally via git. Returns true only if
  ## clone is successful. If the target already exists this function
  ## will return false.
  if not fileExists(path):
    if not git(["clone", url, path]):
      return false
    else: return true

proc update*(path: string): bool =
  ## Change into the target directory and attempt to execute git update.
  ## Returns true only if the update is successful - otherwise false.
  let currentDir = getCurrentDir()
  if path.dirExists():
    if not dirExists(path/".git"):
      return false
    else:
      setCurrentDir(path)
      defer: setCurrentDir(currentDir)
      if git(["pull"]):
        return true

proc init_submodules*: bool =
  ## Initialize submodules if necessary. Will only return false if
  ## the initialization of the submodule system itself fails.
  if not fileExists(".gitmodules"):
    if not git(["submodule", "init"]):
      return false
  return true

proc submodule*(url, path: string): bool =
  ## Download a repository as a git submodule
  ## Will initalize submodules if necessary
  ## Returns true only if the submodule is added successfully

  # Initialize submodules if we have to
  if not init_submodules():
    return false # Something went terribly wrong initializing.
  
  # Add a submodule to the given path
  if not dirExists(path):
    if git(["submodule", "add", url, path]):
      return true

