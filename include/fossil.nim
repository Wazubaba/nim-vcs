import os
import osproc
import strformat

import ../src/exceptions

proc fossil*(args: openArray[string]): bool =
  ## Invoke the fossil program found on the system path
  let fossil = findExe("fossil")
  if fossil.len == 0:
    raise newException(SubprocException, "Cannot locate fossil on system path")

  let fossilproc = startProcess(fossil, getCurrentDir(), args, options={poParentStreams})
  if waitForExit(fossilproc) == 0: return true

