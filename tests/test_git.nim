import git
import os
import unittest

suite("Test git support"):
  if dirExists("clonetest"):
    echo "Deleting old test data"
    removeDir("clonetest")

  test("git clone"):
    echo "Git clone project from gitlab"
    require(git.clone("https://gitlab.com/Wazubaba/nim-vcs", "clonetest"))

  if dirExists("clonetest"):
    echo "Deleting test data"
    removeDir("clonetest")
