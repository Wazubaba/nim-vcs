import nake
import os
import strformat

let tests = [
  "test_git"
]

task "default", "Run default task":
  runTask("test")

task "test", "Build and execute tests":
  for test in tests:
    let source = "tests"/test.changeFileExt("nim")
    let output = "tests"/".build"/test.changeFileExt(ExeExt)
    direShell(nimExe, "c", "-r", &"--out:{output}", source)

task "clean", "Clean built files":
  removeDir("tests"/".build")

